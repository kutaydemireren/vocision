class VocisesController < ApplicationController
  before_action :set_vocise, only: [:show, :edit, :update, :destroy]
  before_action :correct_user, only: :destroy

  # GET /vocises/1
  # GET /vocises/1.json
  def show
  end

  # GET /vocises/new
  def new
    @vocise = Vocise.new
  end

  # GET /vocises/1/edit
  def edit
  end

  # POST /vocises
  # POST /vocises.json
  def create
    @vocise = current_user.vocises.build(vocise_params)

    respond_to do |format|
      if @vocise.save
        flash[:success] = "Vocise was successfully created."
        format.html { redirect_to @vocise }
        format.json { render :show, status: :created, location: @vocise }
      else
        format.html { render :new }
        format.json { render json: @vocise.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /vocises/1
  # PATCH/PUT /vocises/1.json
  def update
    respond_to do |format|
      if @vocise.update(vocise_params)
        flash[:success] = "Vocise was successfully updated."
        format.html { redirect_to @vocise }
        format.json { render :show, status: :ok, location: @vocise }
      else
        format.html { render :edit }
        format.json { render json: @vocise.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /vocises/1
  # DELETE /vocises/1.json
  def destroy
    @vocise.destroy
    respond_to do |format|
      flash[:success] = "Vocise was successfully deleted."
      redirect_to request.referrer || root_url
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_vocise
    @vocise = Vocise.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def vocise_params
    params.require(:vocise).permit(:name, :description, :user_id, :is_private, :picture)
  end

  def correct_user
    @vocise = current_user.vocise.find_by(id: params[:id])
    flash[:danger] = "You are not allowed to perform this action." unless current_user?(@user)
    redirect_to root_url if @vocise.nil?
  end
end
