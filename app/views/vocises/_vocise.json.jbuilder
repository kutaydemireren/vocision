json.extract! vocise, :id, :name, :description, :user_id, :is_private, :created_at, :updated_at
json.url vocise_url(vocise, format: :json)