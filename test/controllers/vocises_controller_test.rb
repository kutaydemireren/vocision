require 'test_helper'

class VocisesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @vocise = vocises(:one)
  end

  test "should get index" do
    get vocises_url
    assert_response :success
  end

  test "should get new" do
    get new_vocise_url
    assert_response :success
  end

  test "should create vocise" do
    assert_difference('Vocise.count') do
      post vocises_url, params: { vocise: { description: @vocise.description, is_private: @vocise.is_private, name: @vocise.name, user_id: @vocise.user_id } }
    end

    assert_redirected_to vocise_url(Vocise.last)
  end

  test "should show vocise" do
    get vocise_url(@vocise)
    assert_response :success
  end

  test "should get edit" do
    get edit_vocise_url(@vocise)
    assert_response :success
  end

  test "should update vocise" do
    patch vocise_url(@vocise), params: { vocise: { description: @vocise.description, is_private: @vocise.is_private, name: @vocise.name, user_id: @vocise.user_id } }
    assert_redirected_to vocise_url(@vocise)
  end

  test "should destroy vocise" do
    assert_difference('Vocise.count', -1) do
      delete vocise_url(@vocise)
    end

    assert_redirected_to vocises_url
  end
end
