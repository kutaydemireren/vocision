class AddIndexToVocisesUserAndCreated < ActiveRecord::Migration[5.0]
  def change
    add_index :vocises, [:user_id, :created_at]
  end
end
