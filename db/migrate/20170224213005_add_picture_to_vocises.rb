class AddPictureToVocises < ActiveRecord::Migration[5.0]
  def change
    add_column :vocises, :picture, :string
  end
end
