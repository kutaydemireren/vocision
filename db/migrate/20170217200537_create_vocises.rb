class CreateVocises < ActiveRecord::Migration[5.0]
  def change
    create_table :vocises do |t|
      t.string :name
      t.string :description
      t.integer :user_id
      t.boolean :is_private

      t.timestamps
    end
  end
end
